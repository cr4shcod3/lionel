#!/usr/bin/python
# -*- coding: utf-8 -*-
# BUILT-IN IMPORTS
import os
import sys
import re
import time
import ast
import socket
# ArgumentParser
from optparse import OptionParser
from optparse import OptionGroup
# Web Scraper
import requests
from bs4 import BeautifulSoup as BS
from selenium import webdriver
# UserAgent
from fake_useragent import UserAgent
ua = UserAgent()
# Unicode Support
from ftfy import fix_text
import colorama
colorama.init()
# Threading
from threading import Thread
from threading import Event
from threading import Lock
event = Event()
lock = Lock()
from multiprocessing.dummy import Pool as ThreadPool
from functools import partial
# Urlparser
if sys.version_info[0] == 3:
    from urllib.parse import urlparse
elif sys.version_info[0] == 2:
    from urlparse import urlparse



def option_parser():
    global options, args
    if sys.version_info[0] == 3:
        usage = fix_text('''

██╗    ██╗███████╗██████╗     ███████╗ ██████╗██████╗  █████╗ ██████╗ ███████╗██████╗ 
██║    ██║██╔════╝██╔══██╗    ██╔════╝██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔══██╗
██║ █╗ ██║█████╗  ██████╔╝    ███████╗██║     ██████╔╝███████║██████╔╝█████╗  ██████╔╝
██║███╗██║██╔══╝  ██╔══██╗    ╚════██║██║     ██╔══██╗██╔══██║██╔═══╝ ██╔══╝  ██╔══██╗
╚███╔███╔╝███████╗██████╔╝    ███████║╚██████╗██║  ██║██║  ██║██║     ███████╗██║  ██║
 ╚══╝╚══╝ ╚══════╝╚═════╝     ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚══════╝╚═╝  ╚═╝

Created By: Cr4sHCoD3

Usage: python {0} [options]

IP Range Scanner / Reverse DNS Lookup:
    $ python {0} -i 1.1.1.1 -t 100
    $ python {0} -i 1.1.1.1/24 -t 100
    $ python {0} -i example.com -t 100

Mass Subdomain and Mass Email Scraper:
    $ python {0} -f ips_list.txt -o subdomains_output.txt -e emails_output.txt
    $ python {0} -f ips_list -o subdomains_output -e emails_output

Email Duplicate Remover:
    $ python {0} -r emails_output.txt
    $ python {0} -r emails_output
'''.format(sys.argv[0]))
    elif sys.version_info[0] == 2:
        usage = '''

██╗    ██╗███████╗██████╗     ███████╗ ██████╗██████╗  █████╗ ██████╗ ███████╗██████╗ 
██║    ██║██╔════╝██╔══██╗    ██╔════╝██╔════╝██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔══██╗
██║ █╗ ██║█████╗  ██████╔╝    ███████╗██║     ██████╔╝███████║██████╔╝█████╗  ██████╔╝
██║███╗██║██╔══╝  ██╔══██╗    ╚════██║██║     ██╔══██╗██╔══██║██╔═══╝ ██╔══╝  ██╔══██╗
╚███╔███╔╝███████╗██████╔╝    ███████║╚██████╗██║  ██║██║  ██║██║     ███████╗██║  ██║
 ╚══╝╚══╝ ╚══════╝╚═════╝     ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚══════╝╚═╝  ╚═╝

Created By: Cr4sHCoD3

Usage: python {0} [options]

IP Range Scanner / Reverse DNS Lookup:
    $ python {0} -i 1.1.1.1 -t 100
    $ python {0} -i 1.1.1.1/24 -t 100
    $ python {0} -i example.com -t 100

Mass Subdomain and Mass Email Scraper:
    $ python {0} -f ips_list.txt -o subdomains_output.txt -e emails_output.txt
    $ python {0} -f ips_list -o subdomains_output -e emails_output

Email Duplicate Remover:
    $ python {0} -r emails_output.txt
    $ python {0} -r emails_output
'''.format(sys.argv[0]).decode('utf-8')
    parser = OptionParser(usage=usage)
    parser.add_option(
        '-t',
        '--threads',
        dest='threads',
        metavar='NUM',
        default=100,
        help='Threads to Spawn (50)')
    scraper = OptionGroup(
        parser,
        'Reverse IP Lookup And Email Scraper',
        'Get domains from IP (Reverse IP Lookup) and Mass Scrape for Emails.')
    scraper.add_option(
        '-f',
        '--file',
        dest='file',
        metavar='FILE',
        help='List File of IPs (ip_list.txt)')
    scraper.add_option(
        '-o',
        '--output',
        dest='output',
        metavar='FILE',
        default='domains_output.txt',
        help='Output File for domains (output.txt)')
    scraper.add_option(
        '-e',
        '--emails-output',
        dest='emails_output',
        metavar='FILE',
        default='d_emails.txt',
        help='Output File for emails from domains (emails_output.txt)')
    remove_dup = OptionGroup(
        parser,
        'Remove Email Duplicates',
        'Remove all email duplicates in a file.')
    remove_dup.add_option(
        '-r',
        '--remove',
        dest='remove',
        metavar='FILE',
        default='Not',
        help='Remove Duplicates from File (emails_list.txt)')
    ip_to_iprange = OptionGroup(
        parser,
        'Remove Email Duplicates',
        'Remove all email duplicates in a file.')
    ip_to_iprange.add_option(
        '-i',
        '--ip-range',
        dest='ip_range',
        metavar='IP',
        default='Not',
        help='IP to IP Range (1.1.1.1)')
    ip_to_iprange.add_option(
        '--ip-results',
        dest='ip_results',
        metavar='FILE',
        help='Store alive IPS to a file (alive_ips.txt)')
    parser.add_option_group(scraper)
    parser.add_option_group(remove_dup)
    parser.add_option_group(ip_to_iprange)
    options, args = parser.parse_args()



def remove_duplicates():
    dup_arg = options.remove
    if '.txt' not in dup_arg:
        dup_arg = dup_arg + '.txt'
    else:
        pass
    dup_file = open(dup_arg, 'r')
    dup_file_contents = []
    for i in dup_file.readlines():
        i = str(i).replace('\n', '')
        dup_file_contents.append(i)
    dup_file.close()
    dup_file_contents = list(set(dup_file_contents))
    dup_file = open(dup_arg, 'w')
    for i in dup_file_contents:
        if 'info' in i or 'contact' in i or 'admin' in i or 'client' in i or 'desk' in i or 'tech' in i or 'support' in i or 'connect' in i or 'name' in i or 'your' in i or 'comment' in i or 'donate' in i or 'ask' in i or 'example' in i or 'license' in i or 'help' in i or 'follow' in i or 'volunteer' in i: # Add more if you want
            try:
                dup_file_contents.remove(i)
                continue
            except:
                continue
        elif '@' not in i: # If there's no @ sign
            try:
                dup_file_contents.remove(i)
                continue
            except:
                continue
        elif '.png' in i or '.jpg' in i or '.gif' in i or '.jpeg' in i: # If there's no @ sign
            try:
                dup_file_contents.remove(i)
                continue
            except:
                continue
        elif '!' in i or '#' in i or '$' in i or '%' in i or '^' in i or '*' in i or '(' in i or ')' in i or '=' in i or '+' in i or '[' in i or ']' in i or ';' in i or '\'' in i or '\\' in i or '<' in i or '>' in i or '/' in i or '?' in i or '|' in i or '"' in i or ':' in i or '}' in i or '{' in i: # If there's a special character
            try:
                dup_file_contents.remove(i)
                continue
            except:
                continue
        else:
            dup_file.write('{0}\n'.format(i))
    dup_file.close()
    sys.exit()



def check_if_host_is_alive(arg1):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(3)
    try:
        s.connect((arg1, 80))
        lock.acquire()
        if sys.version_info[0] == 3:
            print (fix_text('│\t{0}'.format(arg1)))
        elif sys.version_info[0] == 2:
            print ('{0}\t{1}'.format(chr(179), arg1))
        lock.release()
        s.close()
        return arg1
    except socket.timeout:
        pass
    except TimeoutError:
        pass
    except ConnectionRefusedError:
        pass



def ip_range_scanner():
    ip_range_scanner_ip = options.ip_range
    try:
        if '.txt' not in options.ip_results:
            options.ip_results = options.ip_results + '.txt'
        else:
            options.ip_results = options.ip_results
        alive_ips_results = open(options.ip_results, 'w+')
        alive_ips_bool = True
    except:
        print ('[#] - Store IP in a file?')
        if sys.version_info[0] == 3:
            choice = input('\t> ')
        elif sys.version_info[0] == 2:
            choice = raw_input('\t> ')
        if choice.startswith('y') == True or choice.startswith('Y') == True:
            if sys.version_info[0] == 3:
                file_name = input('\tFilename: ')
            elif sys.version_info[0] == 2:
                file_name = raw_input('\tFilename: ')
            print ('\n\n')
            if '.txt' not in file_name:
                file_name = file_name + '.txt'
            else:
                pass
            alive_ips_results = open(file_name, 'w+')
            alive_ips_bool = True
        elif choice.startswith('n') == True or choice.startswith('N') == True:
            alive_ips_bool = False
            print ('\n\n')
            pass
        else:
            alive_ips_bool = False
            print ('\n\n')
            pass
    if '.com' in ip_range_scanner_ip or '.net' in ip_range_scanner_ip or '.org' in ip_range_scanner_ip or '.gov' in ip_range_scanner_ip or '.mil' in ip_range_scanner_ip: # Add more if you want
        domain = urlparse(ip_range_scanner_ip).netloc
        ip = socket.gethostbyname(ip_range_scanner_ip)
        ip = ip
    elif '/24' in ip_range_scanner_ip:
        ip = ip_range_scanner_ip
    else:
        ip = ip_range_scanner_ip
        ip = ip
    if sys.version_info[0] == 3:
        print (fix_text('╭─────({0})'.format(ip)))
        print (fix_text('│'))
    elif sys.version_info[0] == 2:
        print ('{1}{2}{2}{2}{2}{2}({0})'.format(ip, chr(218), chr(196)))
        print ('{0}'.format(chr(179)))
    ip = ip.split('.')
    ips = []
    for i in range(256):
        if i == 0:
            i = 1
        elif i == 1:
            continue
        i = str(i)
        n_ip = '{0}.{1}.{2}.{3}'.format(ip[0], ip[1], ip[2], i)
        ips.append(n_ip)
    threads = int(options.threads)
    pool = ThreadPool(threads)
    check_host = pool.map(check_if_host_is_alive, ips)
    check_host = [x for x in check_host if x is not None]
    if alive_ips_bool == True:
        for i in check_host:
            alive_ips_results.write('{0}\n'.format(i))
    else:
        pass
    if sys.version_info[0] == 3:
        print (fix_text('│'))
        print (fix_text('╰─────(Done)'))
    elif sys.version_info[0] == 2:
        print ('{0}'.format(chr(179)))
        print ('{1}{2}{2}{2}{2}{2}({0})'.format('Done', chr(192), chr(196)))



def validation():
    global pool, ips, output_file, emails_output_file
    if '.txt' not in str(options.file):
        options.file = options.file + '.txt'
    if options.output == None:
        pass
    elif '.txt' not in str(options.output):
        options.output = options.output + '.txt'
    else:
        options.output = options.output
    if options.emails_output == None:
        pass
    elif '.txt' not in str(options.emails_output):
        options.emails_output = options.emails_output + '.txt'
    else:
        options.emails_output = options.emails_output
    # IPs File Argument
    try:
        ips_file = open(options.file, 'r')
        ips_file = str(ips_file.readlines()).replace('\n', '')
        ips = re.findall(r'(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})', ips_file)
    except:
        print ('[!] - You must provide a file list of IP')
        sys.exit()
    # Output File Argument
    try:
        output_file = open(options.output, 'w+')
    except:
        pass
    # Emails Output File Argument
    try:
        emails_output_file = open(options.emails_output, 'a+')
    except:
        pass


def email_scrape(arg1):
    arg1_domain = urlparse(arg1).netloc
    try:
        emails_output_file = open(options.emails_output, 'a+')
    except:
        emails_output_file = open('d_emails.txt', 'a+')
    emails = []
    try:
        email_scrape_requests = requests.get(arg1)
        email_scrape_soup = BS(email_scrape_requests.text, 'html.parser')
        email_scrape_soup = str(email_scrape_soup)
        email_regex = """(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"""
        email_regex = str(email_regex)
        email_scrape = re.findall(email_regex, email_scrape_soup)
        email_scrape = list(set(email_scrape))
        emails = list(set(emails))
        for i in email_scrape:
            if i in emails:
                continue
            else:
                emails_output_file.write('{0}\n'.format(i))
                emails.append(i)
        emails_output_file.close()
    except:
        pass

def page_links(url, scheme):
    links = []
    # Set the origin url and create an origin urlparse
    origin = url
    origin_urlparse = urlparse(origin)
    origin_domain_name = origin_urlparse.netloc
    # Try/Catch Function for the origin url
    try:
        origin_request = requests.get(url, timeout=7.0)
        # Beautiful Soup Object
        origin_soup = BS(origin_request.text, 'html.parser')
        # Find all links in a tag
        for link in origin_soup.find_all('a', href=True):
            link = link['href']
            if '.jpg' in link or '.png' in link or '.jpeg' in link or '.gif' in link or '.ico' in link or '.mp4' in link or '.mp3' in link or '.3gp' in link:
                continue
            if link == 'javascript:void(0)' or link == '#':
                continue
            if link[:2] == '//':
                link = scheme + link
            if '//' not in link and 'http' not in link:
                if link[:1] == '/':
                    link = scheme + origin_domain_name + link
                else:
                    link = scheme + origin_domain_name + '/' + link
            link_urlparse = urlparse(link)
            link_domain_name = link_urlparse.netloc
            if origin_domain_name not in link_domain_name:
                continue
            elif link in links:
                continue
            else:
                links.append(link)
        # Find all links in img tag
        for link in origin_soup.find_all('img', src=True):
            link = link['src']
            if '.jpg' in link or '.png' in link or '.jpeg' in link or '.gif' in link or '.ico' in link or '.mp4' in link or '.mp3' in link or '.3gp' in link:
                continue
            if link == 'javascript:void(0)' or link == '#':
                continue
            if link[:2] == '//':
                link = scheme + link
            if '//' not in link and 'http' not in link:
                if link[:1] == '/':
                    link = scheme + origin_domain_name + link
                else:
                    link = scheme + origin_domain_name + '/' + link
            link_urlparse = urlparse(link)
            link_domain_name = link_urlparse.netloc
            if origin_domain_name not in link_domain_name:
                continue
            elif link in links:
                continue
            else:
                links.append(link)
        # Find all links in input tag
        for link in origin_soup.find_all('input', src=True):
            link = link['src']
            if '.jpg' in link or '.png' in link or '.jpeg' in link or '.gif' in link or '.ico' in link or '.mp4' in link or '.mp3' in link or '.3gp' in link:
                continue
            if link == 'javascript:void(0)' or link == '#':
                continue
            if link[:2] == '//':
                link = scheme + link
            if '//' not in link and 'http' not in link:
                if link[:1] == '/':
                    link = scheme + origin_domain_name + link
                else:
                    link = scheme + origin_domain_name + '/' + link
            link_urlparse = urlparse(link)
            link_domain_name = link_urlparse.netloc
            if origin_domain_name not in link_domain_name:
                continue
            elif link in links:
                continue
            else:
                links.append(link)
        # Find all links in script tag
        for link in origin_soup.find_all('script', src=True):
            link = link['src']
            if '.jpg' in link or '.png' in link or '.jpeg' in link or '.gif' in link or '.ico' in link or '.mp4' in link or '.mp3' in link or '.3gp' in link:
                continue
            if link == 'javascript:void(0)' or link == '#':
                continue
            if link[:2] == '//':
                link = scheme + link
            if '//' not in link and 'http' not in link:
                if link[:1] == '/':
                    link = scheme + origin_domain_name + link
                else:
                    link = scheme + origin_domain_name + '/' + link
            link_urlparse = urlparse(link)
            link_domain_name = link_urlparse.netloc
            if origin_domain_name not in link_domain_name:
                continue
            elif link in links:
                continue
            else:
                links.append(link)
        iwew = int(len(links))
        if iwew < 5:
            pass
        else:
            pool = ThreadPool(iwew)
            email_scrape_thread = pool.map(email_scrape, links)
            lock.acquire()
            if sys.version_info[0] == 3:
                print ('│\t[+] - {0} : Done'.format(url))
            elif sys.version_info[0] == 2:
                print ('{1}\t[+] - {0} : Done'.format(url, chr(179)))
            lock.release()
    except:
        time.sleep(1)
        pass



def bing_reverse_ip_search(arg1, arg2, arg3, arg4, arg5):
    ip = arg1
    iii = arg2
    reverse_ip_domain_results = arg3
    ii = arg4
    header = {
        'user-agent': str(ua.chrome)
    }
    try:
        bing_search_url = 'https://www.bing.com/search?q=ip%3a{0}&first={1}&FORM=PERE{2}'.format(ip, ii, str(iii))
        bing_search_request = requests.get(bing_search_url, headers=header)
        bing_search_soup = BS(bing_search_request.text, 'html.parser')
        bing_search_links_result = bing_search_soup.find_all('li', attrs={'class': 'b_algo'})
        for a in bing_search_links_result:
            a = a.find('a', href=True)['href']
            url = urlparse(a)
            url = '{0}://{1}'.format(url.scheme, url.netloc)
            if url in reverse_ip_domain_results[ip]:
                pass
            else:
                reverse_ip_domain_results[ip].append(url)
                aw = open('bing_tmp.txt','a+')
                lock.acquire()
                aw.write('{0}\n'.format(url))
                aw.close()
                lock.release()
    except:
        pass



def bing_reverse_ip(arg1, arg2):
    reverse_ip_domain_results = arg2
    header = {
        'user-agent': str(ua.chrome)
    }
    ip = arg1
    i = ip
    reverse_ip_domain_results[ip] = []
    ii = 1
    bing_search_url = 'https://www.bing.com/search?q=ip%3a{0}&first={1}&FORM=PERE{2}'.format(i, ii, '')
    bing_search_request = requests.get(bing_search_url, headers=header)
    bing_search_soup = BS(bing_search_request.text, 'html.parser')
    try:
        sb_count1 = bing_search_soup.find('span', attrs={'class': 'sb_count'})
        if sb_count1 == None:
            pass
        else:
            sb_count1 = sb_count1.text
            sb_count1 = sb_count1.replace(',','').replace('Results','').replace('results','')
    except Exception as e:
        print (e)
        sys.exit()
    bing_search_links_result = bing_search_soup.find_all('li', attrs={'class': 'b_algo'})
    if bing_search_links_result == None:
        pass
    else:
        for a in bing_search_links_result:
            a = a.find('a', href=True)['href']
            url = urlparse(a)
            url = '{0}://{1}'.format(url.scheme, url.netloc)
            if url in reverse_ip_domain_results[ip]:
                pass
            else:
                reverse_ip_domain_results[ip].append(url)
    bing_search_page_2 = bing_search_soup.find('a', attrs={'class':'sb_bp', 'aria-label': 'Page 2'})
    if bing_search_page_2 == None or '2' not in bing_search_page_2.text:
        pass
    else:
        pass
    bing_search_url = 'https://www.bing.com/search?q=ip%3a{0}&first={1}'.format(i, sb_count1)
    bing_search_request = requests.get(bing_search_url, headers=header)
    bing_search_soup = BS(bing_search_request.text, 'html.parser')
    sb_count2 = bing_search_soup.find('span', attrs={'class': 'sb_count'})
    if sb_count2 == None:
        pass
    else:
        sb_count2 = sb_count2.text
        try:
            sb_count2 = re.findall(r'\d+', sb_count2)
            if sb_count1 == sb_count2:
                sb_count = sb_count1
            else:
                sb_count = sb_count2[2]
            sb_count_i = []
            iii = []
            ii = 0
            iter = 1
            while iter <= int(sb_count):
                iter += 10
                sb_count_i.append(iter)
                if iter == 11:
                    iii.append('')
                elif iter >= 21:
                    ii += 1
                    iii.append(ii)
        except:
            sb_count = 100
            sb_count_i = []
            iii = []
            ii = 0
            iter = 1
            while iter <= int(sb_count):
                iter += 10
                sb_count_i.append(iter)
                if iter == 11:
                    iii.append('')
                elif iter >= 21:
                    ii += 1
                    iii.append(ii)
        for i in sb_count_i:
            iiii = sb_count_i.index(i)
            iiiii = 1
            try:
                t = Thread(target=bing_reverse_ip_search, args=(ip, iii[iiii], reverse_ip_domain_results, i, iiiii))
                t.start()
            except:
                pass
            iiiii += 1
        t.join()



def main(ips_file):
    threads = options.threads
    reverse_ip_domain_results = {}
    for i in ips_file:
        t = Thread(target=bing_reverse_ip, args=(i, reverse_ip_domain_results))
        t.start()
    t.join()
    if t.isAlive() == False:
        lock.acquire()
        bing_tmp = open('bing_tmp.txt','r')
        bing_tmp_domains = bing_tmp.readlines()
        bing_tmp.close()
        domains = []
        for i in bing_tmp_domains:
            i = i.replace('\n','')
            domains.append(i)
        lock.release()
        for i in domains:
            i_urlparse = urlparse(i)
            domain = i_urlparse.netloc
            scheme = i_urlparse.scheme
            url = '{0}://{1}'.format(scheme, domain)
            t = Thread(target=page_links, args=(url, scheme))
            t.start()
        t.join()
        if t.isAlive() == False:
            os.remove('bing_tmp.txt')



if __name__ == '__main__':
    option_parser()
    if options.remove != 'Not':
        remove_duplicates()
    elif options.ip_range != 'Not':
        ip_range_scanner()
    else:
        validation()
        main(ips)